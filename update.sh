#!/bin/bash

GPUImagePath="/Users/pedro/Projects/GPUImage"

function findAndCopy () {
  directory=$1
  file_type=$2

  for i in $(find $directory -name $file_type); do
    filename=$(basename $i)
    for j in $(find $GPUImagePath -name $filename | grep -Fv 'Mac'); do
      cp -rf $j $i
    done
  done
}

findAndCopy $(pwd) "*.m"
echo "\n"
findAndCopy $(pwd) "*.h"
