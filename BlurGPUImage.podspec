Pod::Spec.new do |s|
  s.name         = "BlurGPUImage"
  s.version      = "0.0.1"
  s.summary      = "A short description of BlurGPUImage."
  s.homepage     = "https://bitbucket.org/alex_f/blurgpuimage"
  s.author       = {"Alex Fedko" => "fedko.a.g"}
  s.license      = "BSD"
  s.author       = { "Alex Fedko" => "fedko.a.g@gmail.com" }
  s.source       = { :git => "https://alex_f@bitbucket.org/alex_f/blurgpuimage.git", :tag => "0.0.1" }
  s.platform      = :ios, '6.0'
  s.requires_arc = true
  s.source_files  = "BlurGPUImage", "BlurGPUImage/**/*.{h,m}"
end
